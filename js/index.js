let user = JSON.parse(window.localStorage.getItem("vlot!-user"))

checkUserIsPresentAndFetchMessages();
setInterval(function () {
        checkUserIsPresentAndFetchMessages()
    },
    // Deze methode wordt uitgevoerd met een interval van 10 seconds
    10000
);

function checkUserIsPresentAndFetchMessages() {
    if (user != null) {
        hideRegisterInfoForm()
        showMessages()
    }
}

function clearUser() {
    user = null
    window.localStorage.removeItem("vlot!-user")
}

function showMessages() {
    fetchMessages().then(messages => displayMessages(messages))
    document.getElementById("messages-card").removeAttribute("hidden")
    document.getElementById("post-message-card").removeAttribute("hidden")
}

function sendMessage() {
    sendMessageToApi({
        "message": document.getElementById("post-message").value,
        "nickname": user.nickname,
        "avatarImage": user.avatarImage
    }).then(() => showMessages())
}

function hideRegisterInfoForm() {
    document.getElementById("register-card").setAttribute("hidden", "true")
    document.getElementById("user-nickname").textContent = user.nickname
    document.getElementById("user-avatar").src = user.avatarImage
    document.getElementById("user-info").removeAttribute("hidden")
}

function showRegisterInfoForm() {
    fetchAvatars().then(avatars => displayAvatars(avatars))
    document.getElementById("register-info").removeAttribute("hidden")
    document.getElementById("register-server").setAttribute("hidden", "true")
}

function register() {
    user = new User(
        document.getElementById("nickname").value,
        document.querySelector('input[name="avatar"]:checked').id,
        document.getElementById("server").value
    );
    window.localStorage.setItem("vlot!-user", JSON.stringify(user));
    hideRegisterInfoForm()
    showMessages()
}

function displayMessages(messages) {
    let messagesDiv = document.getElementById("messages-card");
    messages.forEach(message => messagesDiv.appendChild(message.createCard()))
}

function displayAvatars(fortniteItems) {
    let avatars = document.getElementById("avatars")
    avatars.innerHTML = fortniteItems.map(fortniteItem => fortniteItem.createCard()).join("")
}