class User {

    constructor(nickname, avatarImage, server) {
        this.nickname = nickname
        this.avatarImage = avatarImage
        this.server = server
    }
}