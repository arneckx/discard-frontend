class FortniteItem {

    constructor(name, image) {
        this.name = name
        this.image = image
    }

    createCard() {
        return `<div class="col-3 m-2">
        <input type="radio" name="avatar" class="visually-hidden" id="${this.image}">
        <label for="${this.image}">
            <img src="${this.image}" class="rounded img-thumbnail img-fluid" alt="${this.name}">
        </label>
    </div>`
    }
}