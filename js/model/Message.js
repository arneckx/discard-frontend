class Message {

    constructor(timestamp, message, nickname, avatarImage) {
        this.timestamp = new Date(timestamp);
        this.message = message;
        this.nickname = nickname;
        this.avatarImage = avatarImage;
    }

    createCard() {
        let newMessage = document.createElement('div');
        newMessage.innerHTML = `
        <div class="alert alert-secondary">
            <div>
                <p>${this.timestamp.toLocaleString()} <img src="${this.avatarImage}" alt="avatar-of-sender" width="30" height="24"> ${this.nickname} </p>
            </div>
            <p>${this.message}</p>
        </div>
        `
        return newMessage
    }

}