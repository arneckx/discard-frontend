

function fetchAvatars() {
    return fetch("https://fortnite-api.theapinetwork.com/items/popular",
        {
            method: 'GET'
        })
        .then(response => response.text())
        .then(result => JSON.parse(result).entries[1].entries.map(item => new FortniteItem(item.name, item.images.transparent)))
        .catch(error => console.log('error', error));
}