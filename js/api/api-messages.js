const earliestDate = new Date(-8640000000000000);
let previousFetch = earliestDate

function fetchMessages() {
    return fetch(`${user.server}?afterTimestamp=${previousFetch.toISOString()}`, {method: 'GET'})
        .then(response => response.text())
        .then(result => {
            let messages = JSON.parse(result).map(item => new Message(item.timestamp, item.message, item.nickname, item.avatarImage));
            previousFetch = new Date()
            return messages
        })
        .catch(error => console.log('error', error));
}

function sendMessageToApi(message) {
    return fetch(user.server, {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(message)
    }).catch(error => console.log('error', error));
}